FROM nginx
COPY ./web/* /usr/share/nginx/html/
COPY ./conf.d/* /etc/nginx/conf.d/
